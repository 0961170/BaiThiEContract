﻿using System;
using System.Collections.Generic;
using RestSharp;
using Newtonsoft.Json.Linq;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace Econtract_test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string token = "";
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            var rs = file.ShowDialog();
            if (rs == DialogResult.OK)
            {
                var client = new RestClient("https://apigateway-econtract-staging.vnptit3.vn/auth-service/oauth/token");
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                var jsonBody = @"{ ""grant_type"": ""client_credentials"",  ""client_id"":
                        ""test.client@econtract.vnpt.vn"",  
                        ""client_secret"": ""U30nrmdko76057dz5aQvV9ug0mTsqAQy""
                         }";

                request.AddParameter("application/json", jsonBody, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                token = (string)JObject.Parse(response.Content).SelectToken("access_token");
                if (token != "")
                {
                    string linkfile = file.FileName;
                    CreateContract(linkfile);
                }
            }

        }
        private void CreateContract(string linkfile)
        {

            var client = new RestClient("https://apigateway-econtract-staging.vnptit3.vn/esolution-service/contracts/create-draft-from-file-raw");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "multipart/form-data");
            request.AddHeader("Authorization", "Bearer " + token);
       

            var strjscustomer = @"{
                            ""email"": ""KH1234@gmail.com"",
                ""sdt"": ""094888889"",
                ""userType"": ""CONSUMER"",
                ""ten"": ""vinb.dlc"",
                ""noiCap"": ""dlk"",
                ""tenToChuc"": ""VNPT DLK"",
                ""mst"": ""111111111"",
                ""loaiGtId"": ""1"",
                ""noiCap"": """",
                ""soDkdn"": """",
                ""ngayCapSoDkdn"": ""2021-12-22"",
                ""noiCapDkkd"": """"
              }";

            string strjscontract = @" {
                ""autoRenew"": ""true"",
                  ""callbackUrl"": ""test url"",
                  ""contractValue"": ""20000"",
                  ""creationNote"": """",
                  ""endDate"": ""2021-11-17"",
                  ""flowTemplateId"": ""b611cb60-e9c0-477e-8a2c-b84a8b4c688d"",
                  ""sequence"": 2,
                  ""signFlow"": [
                    {
                                    ""signType"": ""DRAFT"",
                      ""signForm"": [
                        ""OTP"",
                        ""EKYC"",
                        ""OTP_EMAIL"",
                        ""NO_AUTHEN"",
                        ""EKYC_EMAIL"",
                        ""USB_TOKEN"",
                        ""SMART_CA""
                      ],
                      ""userId"": ""nhansu01@yopmail.com"",
                      ""sequence"": 1,
                      ""limitDate"": 3
                    },
                    {
                                    ""signType"": ""APPROVE"",
                      ""signForm"": [
                        ""OTP"",
                        ""EKYC"",
                        ""OTP_EMAIL"",
                        ""NO_AUTHEN"",
                        ""EKYC_EMAIL"",
                        ""USB_TOKEN"",
                        ""SMART_CA""
                      ],
                      ""departmentId"": """",
                      ""userId"": ""nhansu01@yopmail.com"",
                      ""sequence"": 2,
                      ""limitDate"": 3
                    }
                  ],
                  ""signForm"": [
                     ""USB_TOKEN"",
                       ""SMART_CA""
                      ],
                  ""templateId"": ""632d7d61f62f39e31a22776f"",
                  ""title"": ""test create contract"",
                  ""validDate"": ""2021-11-17"",
                  ""verificationType"": ""NONE"",
                  ""fixedPosition"": false
                    }";
            request.AddParameter("customer", strjscustomer);
            request.AddParameter("contract", strjscontract);
            request.AddFile("", linkfile, "application/pdf");
            request.AddParameter("fields", "{}");
            IRestResponse response = client.Execute(request);
          
            var kq = (string)JObject.Parse(response.Content).SelectToken("message");
            if (kq == "ECT-00000000")
            {
                lblidcontract.Text = (string)JObject.Parse(response.Content).SelectToken("object").SelectToken("contractId");
                txtTaoHopDong.Text = "Tạo thành công hợp đồng";
            }
            else
            {
                txtTaoHopDong.Text = "Không tạo được";

            }    

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(lblidcontract.Text))
            {
                MessageBox.Show("Chưa tạo hợp đồng");
                return;
            }
            var client = new RestClient($"https://apigateway-econtract-staging.vnptit3.vn/esolution-service/contracts/{lblidcontract.Text}/submit-contract");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "Bearer " + token);
            IRestResponse response = client.Execute(request);
            var stt = response.StatusCode;
           if (stt.ToString() == "OK")
            {
                var kq = (string)JObject.Parse(response.Content).SelectToken("message");
                if (kq == "ECT-00000000")
                {
                    txtGuiHd.Text = "Đã gửi HĐ";
                }
                else
                {
                    txtGuiHd.Text = "Không gửi được kiểm tra trạng thái";

                }
            }

            else
            {
                txtGuiHd.Text = "Không gửi được";

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(lblidcontract.Text))
            {
                MessageBox.Show("Chưa tạo hợp đồng");
                return;
            }
         
            var client = new RestClient($"https://apigateway-econtract-staging.vnptit3.vn/esolution-service/contracts/{lblidcontract.Text}/digital-sign");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddFile("", @"C:\Users\ViNb\Desktop\testfile.pdf", "application/pdf");
            var bodysign = new
            {
                SignForm = "OTP_EMAIL",
                name = "Nguyễn Bá Vị",
                taxCode = "1231231234",
                provider = "VNPTDLK",
                identifierCode = "123123123",
                phone = "09999999",
                email = "0961170@gmail.com",
                status = "VALID",
                signType = "APPROVAL",
                ekycInfo = "APPROVAL"
            };
            request.AddParameter("data", JsonConvert.SerializeObject(bodysign));
            IRestResponse response = client.Execute(request);
            
        }
    }
}
